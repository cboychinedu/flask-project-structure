#!/usr/bin/env python3

# Importing the necessary modules
import os 
from flask import Flask, url_for
from routes.users import UsersRoutes
from routes.admin import AdminRoutes
from routes.home import HomeRoutes
from flask import render_template
from flask_script import Manager

# Creating an instance of the Routes class
usersRoutes = UsersRoutes()
adminRoutes = AdminRoutes()
homeRoutes = HomeRoutes()

# Creating the flask application instance
app = Flask(__name__)
app.config['SECRET_KEY'] = "skensinfw43854dn8)(*&$#*#@@!_%$#!";

# Allow the flask application to accept command line arguments
manager = Manager(app)

# Adding routes for the home section
app.add_url_rule("/clear", "clearflashmessage", homeRoutes.clear, methods=["GET"]); 
app.add_url_rule("/", "homePage", homeRoutes.index, methods=["GET"]);
app.add_url_rule("/templates", "templatesRoute", homeRoutes.templates, methods=["GET"]);
app.add_url_rule("/cookies", "cookiesRoute", homeRoutes.cookies, methods=["GET"]);
app.add_url_rule("/redirect", "redirectRoute", homeRoutes.redirect, methods=["GET"]);
app.add_url_rule("/user/<name>", "routeUsernameParser", homeRoutes.user, methods=["GET"]);

# Adding routes for the admin user
app.add_url_rule("/admin/home", "adminHome", adminRoutes.home, methods=["GET"]);
app.add_url_rule("/admin/settings", "adminSettings", adminRoutes.settings, methods=["GET"]);

# Adding routes for the users
app.add_url_rule("/users/signup", "signup", usersRoutes.signup, methods=["GET"]);
app.add_url_rule("/users/signin", "signin", usersRoutes.signin, methods=["GET"]);
app.add_url_rule("/users/update_profile", "update_profile", usersRoutes.update_profile, methods=["GET"]);
app.add_url_rule("/users/dashboard", "dashboard", usersRoutes.dashboard, methods=["GET"]);


# Handling custom error pages
@app.errorhandler(404)
def page_not_found(e):
    # Execute this route if the user navigates to a route that does not exist
    return render_template('404.html'), 404;


# Handling error request 500
@app.errorhandler(500)
def internal_server_error(e):
    # Execute this route if the request generated gives an internal server error
    return render_template('500.html'), 500;

# Adding functions for updating the web application on reload 
@app.context_processor
def override_url_for():
    return dict(url_for=dated_url_for)

# Creating a function called dated url for tracking the changes made
def dated_url_for(endpoint, **values):
    if endpoint == 'static':
        filename = values.get('filename', None)
        if filename:
            file_path = os.path.join(app.root_path,
                                 endpoint, filename)
            values['q'] = int(os.stat(file_path).st_mtime)
    return url_for(endpoint, **values)


# Running the flask server 
if __name__ == "__main__":
    # manager.run()
    app.run(port=3001,
            host="localhost",
            debug=True);


