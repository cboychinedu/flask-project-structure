# Creating the first route
# @app.route("/", methods=["POST", "GET"])
# def index():
#     # Getting the request headers from the request body
#     # user_agent = request.headers.get("User-Agent");
#     print(request);
#
#     # Execute the block of code below if this route is accessed
#     return "<h1> Hello world! </h1>", 200;
#
# # Rendering templates files "Html files"
# @app.route('/templates', methods=["GET"])
# def templates():
#     # This route would render the "index.html" file found in the templates dir
#     return render_template('index.html', name="Chinedum Mbonu",
#                              message="Test321"), 200
#
#
# # Adding cookies into the response body
# @app.route('/cookies', methods=["GET"])
# def cookies():
#     # Creating a response variable
#     response = make_response('<p> This document carries a cookie! </p>')
#     response.set_cookie("answer", "42")
#
#     # Returning the response
#     return response, 200
#
# # Adding a route for redirection
# @app.route('/redirect', methods=["GET"])
# def redirect():
#     # Redirect the user to the cookies route
#     return redirect('/cookies')
#
# # Adding names into the url routes
# @app.route("/user/<name>", methods=["GET"])
# def user(name):
#     return f"<p> Your name is {name}", 200;
