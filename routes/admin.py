#!/usr/bin/env python3

# Creating a class for holding specific routes for the admin
class AdminRoutes:
    # Creating the admin home page
    def home(self):
        # this is the url route for the home admin
        return "<h3> Admin home page route </h3>";

    # Creating the settings page
    def settings(self):
        # this is the url route for the settings page
        return "<h3> Admin settings page route </h3>";