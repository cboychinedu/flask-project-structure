#!/usr/bin/env python3

# Creating a class for holding specific routes addresses
class UsersRoutes:
    def __init__(self):
        pass

    # Sign up route
    def signup(self):
        return "<h3> Signup route </h3>", 200;

    # Sign in route
    def signin(self):
        return "<h3> Signin route </h3>", 200;

    # Update profile route
    def update_profile(self):
        return "<h3> Update profile route </h3>", 200;

    # Users dashboard (profile)
    def dashboard(self):
        return "<h3> User's dashboard(Profile) </h3>", 200;
