#!/usr/bin/env python3

# Importing the necessary modules
from flask import request
from wtforms import Form 
from wtforms import StringField, SubmitField, PasswordField
from wtforms.validators import Required
from flask import session, redirect, url_for
from flask import render_template, make_response, flash 

# Creating a class for the name form 
class NameForm(Form):
    name = StringField("What is your name?", validators=[Required()]); 
    submit = SubmitField("Submit"); 
    password = PasswordField("Type your password here!", validators=[Required()]); 

# Creating an instance of the NameForm class
form = NameForm(); 

stores = {
    "Keg Spar": 12.009, 
    "Ketchup": 34.56
}

# Creating a class for home routes
class HomeRoutes:
    # Clear flash messages 
    def clear(self):
        # 
        session.pop('_flashes', None)
        return render_template('index.html', form=form); 


    # Home page
    def home(self):
        # Getting the request headers from the request body
        user_agent = request.headers.get("User-Agent");
        return user_agent, 200;
        # return "<h3> Hello world! </h3>", 200;

    # Static files
    def index(self):
        # 
        # if form.validate():
        #     name = form.name.data 
        #     print(name); 
        
        # Adding flash message 
        flash("Welcome to our website."); 

        # Rendering the index.html template file
        return render_template('index.html', form=form);

    # Rendering templates files "Html files"
    def templates(self):
        # methodRequest = request.body
        # This route would render the "index.html" file found in the
        # templates directory
        return render_template('index.html', name="Chinedum Mbonu", message="Test321"), 200;

    # Cookies routes for saving cookies into the client browser
    def cookies(self):
        # Creating a response variable
        response = make_response("<p> This document carries a cookie! </p>");
        response.set_cookie("answer", "42");

        # Returning the response
        return response, 200;

    # Adding route for re-direction
    def redirect(self):
        # Redirect the user to the cookies route
        return redirect('/cookies');

    # Adding a route for re-direction
    def user(self, name):
        # Creating a function for taking values parsed into it as arguments
        return f"<p> Your name is {name}", 200;
